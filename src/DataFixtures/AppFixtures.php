<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker::create('fr_FR');

        // Création de 1000 Authors
        $authors = [];
        for ($i = 0; $i < 1000; ++$i) {
            $author = new Author();
            $author->setName($faker->name);
            $manager->persist($author);
            $authors[] = $author;
        }

        // création de 1000 Maisons éditions
        $publishers = [];
        for ($i = 0; $i < 1000; ++$i) {
            $publisher = new Publisher();
            $publisher->setName($faker->name);
            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        // Création des status
        $status = [];
        foreach (['à lire', 'en cours', 'lu'] as $value) {
            $oneStatus = new Status();
            $oneStatus->setName($value);
            $manager->persist($oneStatus);
            $status[] = $oneStatus;
        }

        // Création de 10000 livres
        $books = [];
        for ($i = 0; $i < 1000; ++$i) {
            /** @phpstan-ignore-next-line  */
            $isbn10 = $faker->isbn10;
            /** @phpstan-ignore-next-line  */
            $isbn13 = $faker->isbn13;
            $book = new Book();
            $book
                ->setGoogleBooksId($faker->uuid)
                ->setTitle($faker->sentence)
                ->setSubtitle($faker->sentence)
                ->setPublishDate($faker->dateTime)
                ->setDescription($faker->text)
                ->setIsbn10($isbn13)
                ->setIsbn13($isbn13)
                ->setPageCount($faker->numberBetween(100, 1000))
                ->setThumbnail('https://picsum.photos/200/300')
                ->setSmallThumbnail('https://picsum.photos/100/150')
                ->addAuthor($faker->randomElement($authors))
                ->addPublisher($faker->randomElement($publishers));
            $manager->persist($book);
            $books[] = $book;
        }

        // Création de 10 Utilisateurs
        $users = [];
        for ($i = 0; $i < 100; ++$i) {
            $user = new User();
            $user->setEmail($faker->email);
            $user->setPassword($faker->password);
            $user->setPseudo($faker->userName);
            $manager->persist($user);
            $users[] = $user;
        }

        // Creation de 1000 Userbooks par user
        foreach ($users as $user) {
            for ($i = 0; $i < 100; ++$i) {
                $userBook = new UserBook();
                $userBook
                    ->setReader($user)
                    ->setStatus($faker->randomElement($status))
                    ->setRating($faker->numberBetween(0, 5))
                    ->setComment($faker->text)
                    ->setBook($faker->randomElement($books))
                    ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime))
                    ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime));
                $manager->persist($userBook);
            }
        }

        $manager->flush();
    }
}
